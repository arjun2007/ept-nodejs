const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
var moment = require('moment-timezone');
moment.tz.add("Asia/Calcutta|HMT BURT IST IST|-5R.k -6u -5u -6u|01232|-18LFR.k 1unn.k HB0 7zX0");
moment.tz.link("Asia/Calcutta|Asia/Kolkata");

const schema = new Mongoose.Schema({
  userId: { type: Schema.Types.ObjectId, ref: 'User' },
  type: { type: String, enum: ['D', 'C',], trim: true }, // C-Credit, D-Debit
  narration: { type: String, trim: true },
  amount: { type: Number, default: 0 },
  finalAmount: { type: Number, default: 0 },
  transactionId: { type: String },
  orderId: { type: String, default: '' },
  paymentId: { type: String, default: '' }
}, { timestamps: true });

schema.virtual('date').get(function () {
  let date = moment(this.createdAt).tz('Asia/Calcutta').format('YYYY-MM-DD')
  return date
});

schema.virtual('time').get(function () {
  let time = moment(this.createdAt).tz('Asia/Calcutta').format('hh mm a')
  return time;
});

module.exports = Mongoose.model('Transaction', schema);
