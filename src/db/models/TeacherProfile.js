const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const TeacherProfileSchema = new Mongoose.Schema({
    teacherId: { type: Schema.Types.ObjectId, ref: 'User' },
    degree: { type: String, default: '' },
    description: { type: String, default: "" },
    subjects: [{ type: String }],
})

module.exports = Mongoose.model('TeacherProfile', TeacherProfileSchema);
