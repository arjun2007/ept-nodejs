const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const StudentProfileSchema = new Mongoose.Schema({
    studentId: { type: Schema.Types.ObjectId, ref: 'User' },
    education: { type: String, default: '' },
    qualification: { type: String, default: "" },
    languages: [{ type: String }],
})

module.exports = Mongoose.model('StudentProfile', StudentProfileSchema);
