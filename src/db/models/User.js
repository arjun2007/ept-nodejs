const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const UserSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  name: { type: String, required: true },
  mobile: { type: Number, default: null },
  userType: { type: String, enum: ['student', 'teacher'], required: true },
  password: { type: String, required: true },
  salt: { type: String, required: true },
  walletAmount: { type: Number, default: 0 },
  otpData: {
    _id: false,
    otp: { type: Number, default: 0 },
    expireTime: { type: Number, default: 0 }
  },
  resetPassOTPData: {
    _id: false,
    otp: { type: Number, default: 0 },
    expireTime: { type: Number, default: 0 }
  },
  device: {
    _id: false,
    type: { type: String, default: '' },
    id: { type: String, default: '' }
  },
  isVeifiedOTP: { type: String, default: "no" },
  created: { type: Date, default: new Date() },
  updated: { type: Date, default: new Date() },
  deleted: { type: String, enum: ['yes', 'no'], default: 'no' },
  resetPasswordToken: { type: String },
  resetPasswordExpires: { type: Date },
  online: { type: String, enum: ['yes', 'no'], default: "no" },
  userType: { type: String, enum: ['student', 'teacher'], required: true }
});

UserSchema.methods.setPassword = function (password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.password = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

UserSchema.methods.validatePassword = function (password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return this.password === hash;
};

UserSchema.methods.generateJWT = function () {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 60);

  return jwt.sign({
    email: this.email,
    id: this._id,
    exp: parseInt(expirationDate.getTime() / 1000, 10),
  }, 'secret');
}

UserSchema.methods.toAuthJSON = function () {
  return {
    _id: this._id,
    email: this.email,
    token: this.generateJWT(),
  };
};

module.exports = mongoose.model('User', UserSchema);