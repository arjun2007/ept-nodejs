require('dotenv').config()
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
var moment = require('moment-timezone');
moment.tz.add("Asia/Calcutta|HMT BURT IST IST|-5R.k -6u -5u -6u|01232|-18LFR.k 1unn.k HB0 7zX0");
moment.tz.link("Asia/Calcutta|Asia/Kolkata");

const schema = new Mongoose.Schema({
  conversationId: { type: Schema.Types.ObjectId, ref: 'Conversations' },
  body: { type: String, trim: true },
  author: { type: Schema.Types.ObjectId, ref: 'User' },
  localUid: { type: String, trim: true },
  msgType: { type: String, trim: true, required: true, enum: ['n', 'a'] }, // n->normal message, a->attachment message
  attachment: {
    name: { type: String, trim: true },
    type: { type: String, trim: true },
    size: { type: String, trim: true },
    url: { type: String, trim: true }
  },
  localUidDateTime: {
    date: { type: String },
    time: { type: String }
  },
  delivery: [{
    userId: { type: String },
    date: { type: String },
    time: { type: String }
  }],
  received: [{
    userId: { type: String },
    date: { type: String },
    time: { type: String }
  }],
}, { timestamps: true })

schema.virtual('date').get(function () {
  let date = moment(this.createdAt).tz('Asia/Calcutta').format('YYYY-MM-DD')
  return date
});

schema.virtual('time').get(function () {
  let time = moment(this.createdAt).tz('Asia/Calcutta').format('hh mm a')
  return time;
});

schema.virtual('publicUrl').get(function () {
  const url = this.attachment.url ? process.env.APP_HOST + this.attachment.url : ''
  return url;
});
module.exports = Mongoose.model('Messages', schema);
