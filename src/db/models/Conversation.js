const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const schema = new Mongoose.Schema({
  name: { type: String, trim: true },
  type: { type: String, enum: ['alert', 'chat'], trim: true },
  studentId: { type: Schema.Types.ObjectId, ref: 'User' },
  members: [{ type: Schema.Types.ObjectId, ref: 'User' }] //student with all teacher with relevant subject
}, { timestamps: true });

module.exports = Mongoose.model('Conversations', schema);
