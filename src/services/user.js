const jwt = require('jsonwebtoken')
const User = require('../db/models/User')
const StudentProfile = require('../db/models/StudentProfile')
const TeacherProfile = require('../db/models/TeacherProfile')
const Transaction = require('../db/models/Transaction')
const aeh = require('../../src/middleware/asyncErrorHandler');

//Function to validate email
const validateEmail = function validateEmail(mail, res) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return true
    }
    return false
}

//Function to validate Phone Number
const validatePhonenumber = function (inputtxt) {
    var phoneno = /^\d{10}$/;
    if (inputtxt.match(phoneno)) {
        return true;
    }
    else {
        return false;
    }
}

// function to check if email already exists for another user
const emailDuplicacyCheck = function (email) {
    return new Promise(function (resolve, reject) {
        var where = { 'email': email };
        User.find(where)
            .exec()
            .then((data) => {
                resolve(data)
            })
            .catch((e) => {
                reject(new Error(e));
            })
    })
}

// function to create table for profile of student and teacher
const AddProfileData = function (id, data) {
    return new Promise(aeh(async function (resolve, reject) {
        if (data.userType == 'student') {
            const education = data.education ? data.education : ''
            const qualification = data.qualification ? data.qualification : ''
            const languages = data.languages ? data.languages : []
            const createStudntProfile = await StudentProfile.create({ studentId: id, education, qualification, languages })
            resolve(createStudntProfile)
        }
        if (data.userType == 'teacher') {
            const degree = data.degree ? data.degree : ''
            const description = data.description ? data.description : ''
            const subjects = data.subjects ? data.subjects : []
            const createTeacherProfile = await TeacherProfile.create({ teacherId: id, degree, description, subjects })
            resolve(createTeacherProfile)
        }
    }))
}

// function to find account by Id
const getDataById = function (id) {
    return new Promise(function (resolve, reject) {
        var where = { '_id': id };
        User.find(where)
            .exec()
            .then((data) => {
                resolve(data)
            })
            .catch((e) => {
                reject(new Error(e));
            })
    })
}

// function to find studentProfile by Id
// const getSudentProfileData = function (id) {
//     return new Promise(function (resolve, reject) {
//         var where = { '_id': id };
//         User.find(where)
//             .exec()
//             .then((data) => {
//                 resolve(data)
//             })
//             .catch((e) => {
//                 reject(new Error(e));
//             })
//     })
// }

// function to check if Phone already exists for another user
const phoneDuplicacyCheck = function (mobile) {
    return new Promise(function (resolve, reject) {
        var where = { 'mobile': mobile };
        User.find(where)
            .exec()
            .then((data) => {
                resolve(data)
            })
            .catch((e) => {
                reject(new Error(e));
            })
    })
}

//function to save data of a new Student 
const addNewUser = function (params) {
    var response = { status: 200, msg: "Success", data: [] };
    try {
        var finalUser = new User(params);
        finalUser.setPassword(params.password);
        finalUser.save();
        response['data'] = finalUser;
        return response;
    } catch (err) {
        resposne['status'] = 500;
        response['msg'] = "Failed";
        response['data'] = err;
    }
    return response;
}


//function to update status of OTP
const getUpdateVerification = function (email) {
    return new Promise(function (resolve, reject) {
        User.findOneAndUpdate(
            { 'email': email },
            {
                $set: { isVeifiedOTP: 'yes' }
            },
            {
                upsert: true,
                new: true,
                runValidators: true,
                rawResult: true,
                setDefaultsOnInsert: true,
                useFindAndModify: false
            },
            function (err, doc) { // callback
                if (err) {
                    console.log("Error While Updating OTP verification status", err);
                    reject(new Error(err));
                } else {
                    resolve(doc)
                }
            }
        )
    })
}

//function to update OTP details
const getUpdateOPT = function getUpdateOPT(email, otp) {
    return new Promise(function (resolve, reject) {
        var currentTimeStamp = Math.floor(Date.now() / 1000)
        var expireTimeStamp = currentTimeStamp + 600
        var otpData = {
            otp: otp,
            expireTime: expireTimeStamp
        }
        User.findOneAndUpdate(
            { 'email': email },
            {
                $set: { otpData: otpData }
            },
            {
                upsert: true,
                new: true,
                runValidators: true,
                rawResult: true,
                setDefaultsOnInsert: true,
                useFindAndModify: false
            },
            function (err, doc) { // callback
                if (err) {
                    console.log("Error While Updating OTP", err);
                    reject(new Error(err));
                } else {
                    console.log("OTP Updated");
                    resolve(doc)
                }
            }
        )

    })
}

//function to update Password Reset OTP details
const updatePassResetOPT = function (id, otp) {
    return new Promise(function (resolve, reject) {
        var currentTimeStamp = Math.floor(Date.now() / 1000)
        var expireTimeStamp = currentTimeStamp + 600
        var resetPassOTPData = {
            otp: otp,
            expireTime: expireTimeStamp
        }
        User.findOneAndUpdate(
            { _id: id },
            {
                $set: { resetPassOTPData: resetPassOTPData }
            },
            {
                upsert: true,
                new: true,
                runValidators: true,
                rawResult: true,
                setDefaultsOnInsert: true,
                useFindAndModify: false
            },
            function (err, doc) { // callback
                if (err) {
                    console.log("Error While Updating Password Reset OTP", err);
                    reject(new Error(err));
                } else {
                    console.log("Password reset OTP Updated");
                    resolve(doc)
                }
            }
        )

    })
}



//function to generate JWT token while creating user account which is used for authetication 
const generateToken = function (mongoId, email) {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);

    return jwt.sign({
        email: email,
        id: mongoId,
        exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, 'secret');
}


const getAddTransactionTable = function (id) { //id should be userId
    return new Promise(async function (resolve, reject) {
        if (id) {
            var saveObj = new Transaction({
                userId: id ? id : '',
                type: 'C',
                narration: 'Account Created',
                transactionId: Math.floor(new Date().getTime() / 1000),
            })
            saveObj.save(function (err, doc) {
                if (err) {
                    console.log("Error While Adding New Transacton Table")
                    reject(err)
                }
                console.log("New Transacion Table Added successfully")
                resolve(doc);
            })
        } else {
            console.log("Error! Did not get user ID while adding Transaction Table!")
        }
    })
}

module.exports = {
    validateEmail,
    validatePhonenumber,
    emailDuplicacyCheck,
    getDataById,
    phoneDuplicacyCheck,
    addNewUser,
    getUpdateVerification,
    getUpdateOPT,
    generateToken,
    AddProfileData,
    getAddTransactionTable,
    updatePassResetOPT,
    // getSudentProfileData
}