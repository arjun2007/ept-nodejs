const express = require('express');
const router = express.Router();
const fileUpload = require('express-fileupload');
const Message = require('../db/models/Message');
const Coversation = require('../db/models/Conversation');
const aeh = require('../../src/middleware/asyncErrorHandler');
const path = require('path');

// Save Message in db

router.post('/save-message', aeh(async function (req, res) {
    const message = await Message.create(req.body);
    res.success("Message Saved Successfully", message)
}));

// Find Message in db  using msg Id

router.get('/message/:id', aeh(async function (req, res) {
    const { id } = req.params
    const message = await Message.findOne({ _id: id });
    res.success("Message found Successfully", message)
}));

//Get Sorted Messages By userId
router.get('/:userId/recent-chats', aeh(async function (req, res) {
    const { userId } = req.params;
    const allCoversation = await Coversation.find({ "members": userId }).populate("members");
    const messagesPromise = allCoversation.map(async ({ _id: conversationId, type: conversationType, members, name }) => {
        let friend = null;
        for (const member of members) {
            if (member._id.toString() !== userId) {
                friend = member;
            }
        }

        const latestMessageOfConversation = await Message.findOne({ conversationId }).populate('author').sort({ createdAt: -1 })
        const convObj = {
            conversationId,
            conversationType,
            friend,
            name,
            members
        };

        return !!latestMessageOfConversation
            ? {
                ...convObj,
                msg: latestMessageOfConversation.body,
                author: latestMessageOfConversation.author,
                createdAt: latestMessageOfConversation.createdAt,
                time: latestMessageOfConversation.time,
                date: latestMessageOfConversation.date
            }
            : convObj;
    });

    const messages = await Promise.all(messagesPromise);
    if (messages && messages.length > 0) {
        messages.sort(function compare(a, b) {
            let dateA = new Date(a.createdAt);
            let dateB = new Date(b.createdAt);
            return dateA - dateB;
        });
        res.success("Chats found successfully", messages);
    } else {
        return res.success("No chats found")
    }
}));

// API to upload file
router.post('/upload', aeh(async function (req, res) {
    if (!req.files.files) {
        return res.status(404).error("No files were uploaded")
    }
    const sampleFile = req.files.files;
    const filePath = `./public/uploads/${sampleFile.name}`
    const url = `/uploads/${sampleFile.name}`
    const attachment = {
        name: sampleFile.name,
        size: sampleFile.size,
        type: sampleFile.mimetype,
        url: url,
    }
    await sampleFile.mv(filePath)
    return res.status(200).success("File Uploaded", attachment)
}));

module.exports = router;