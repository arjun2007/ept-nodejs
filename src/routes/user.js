require('dotenv').config()
const express = require('express')
const passport = require('passport')
const nodemailer = require('nodemailer');
const userRouter = express.Router();
const Config = require('../config/config');
const User = require('../db/models/User');
const StudentProfile = require('../db/models/StudentProfile');
const TeacherProfile = require('../db/models/TeacherProfile');
var Service = require('../services/user');
const aeh = require('../../src/middleware/asyncErrorHandler');

const mailTransporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.GMAIL_ID,
    pass: process.env.GMAIL_PASSWORD
  }
});

//=================API to get all users==================//
userRouter.get('/', aeh(async function (req, res) {
  const allUsers = await User.find();
  return res.success("Users Found", allUsers);
}))

//=============API TO GENERATE OTP FOR STUDENA ADN TEACHER====================//
userRouter.post('/generateOTP', async function (req, res) {
  var data = req.body;
  if (!data.email || !data.name || !data.mobile || !data.password || !data.confPassword || !data.deviceType || !data.deviceId || !data.userType) {
    return res.status(200).json({
      success: false,
      msg: 'Please pass all Data in body'
    })
  }
  var params = {
    name: data.name ? data.name : '',
    email: data.email ? data.email : '',
    mobile: data.mobile ? data.mobile : '',
    userType: data.userType ? data.userType : '',
    password: data.password ? data.password : '',
    confPassword: data.confPassword ? data.confPassword : '',
    device: {
      type: data.deviceType ? data.deviceType : '',
      id: data.deviceId ? data.deviceId : ''
    }
  };

  if (data.password !== data.confPassword) {
    return res.status(200).json({ success: false, msg: 'Error! Passwords do not match!' })
  }
  //validate Email
  var checkEmail = await Service.validateEmail(params['email'])
  if (!checkEmail) {
    return res.status(200).json({ success: false, msg: 'Error! Email is not Valid!' })
  }
  //validate Phone Number
  var checkPhone = await Service.validatePhonenumber(params['mobile'])
  if (!checkPhone) {
    return res.status(200).json({ success: false, msg: 'Error! Phone No. is not Valid!' })
  }
  // check email duplicacy
  var duplicateEmailId = await Service.emailDuplicacyCheck(params['email'])
  if (duplicateEmailId && duplicateEmailId.length > 0) {
    return res.status(200).json({ success: false, msg: 'Error! Email Already Exist!' })
  }
  // check phone duplicacy
  // var duplicatePhone = await Service.phoneDuplicacyCheck(params['mobile'])
  // if (duplicatePhone && duplicatePhone.length > 0) {
  //   return res.status(200).json({ success: false, msg: 'Error! Phone Already Exist!' })
  // }

  // create OTP With Twilio
  // var client = require('twilio')(Config.accountSid, Config.authToken);
  var otp = Math.floor(100000 + Math.random() * 900000);
  // client.messages
  //   .create({
  //     body: 'Your Veification otp is ' + otp + ' valid for 10 minutes',
  //     from: '+19382010415',
  //     to: '+91' + params['mobile']
  //   })
  //   .then(async message => {

  //     var result = await Service.addNewUser(params);
  //     if (result && result.data) {
  //       const addProfileData = await Service.AddProfileData(result.data._id, req.body)
  //       var generateTransaction = await Service.getAddTransactionTable(result.data._id)
  //     }

  //     var updateOTP = await Service.getUpdateOPT(params['email'], otp)
  //     return res.status(200).json({ success: true, msg: 'OTP sent successfully!' })
  //   })
  //   .catch((err) => {
  //     console.log("errrrrrrrrrrrrr", err)
  //   })

  let mailDetails = {
    from: 'amanraza2507@gmail.com',
    to: params['email'],
    subject: 'EPT Registration',
    text: 'OTP for account verification is ' + otp + ' valid for 10 minutes'
  };

  await mailTransporter.sendMail(mailDetails, async function (err, data) {
    if (err) {
      return res.status(401).json({ success: false, msg: 'Error while sending OTP!' })

    } else {
      var result = await Service.addNewUser(params);
      if (result && result.data) {
        const addProfileData = await Service.AddProfileData(result.data._id, req.body)
        var generateTransaction = await Service.getAddTransactionTable(result.data._id)
      }

      var updateOTP = await Service.getUpdateOPT(params['email'], otp)
      return res.status(200).json({ success: true, msg: 'OTP sent successfully!' })
    }
  });

});

//=============Verify OTP for student anf teacher account====================//
userRouter.post('/verifyOTP', async function (req, res) {
  var data = req.body;
  if (!data.email || !data.otp) {
    return res.status(200).json({
      success: false,
      msg: 'Please pass email and otp in body'
    })
  }

  var getUserData = await Service.emailDuplicacyCheck(data.email)
  if (!getUserData || getUserData.length == 0) {
    return res.status(200).json({ success: false, msg: 'Error! Account not found!' })
  }

  var otpData = getUserData && getUserData.length > 0 ? getUserData[0].otpData : {}
  var actualOTP = otpData.otp ? otpData.otp : 0
  var expireTime = otpData.expireTime ? otpData.expireTime : 0

  var currentTimeStamp = Math.floor(Date.now() / 1000)

  if ((currentTimeStamp > expireTime) || actualOTP !== Number(data.otp)) {
    return res.status(200).json({ success: false, msg: 'Error! Invalid OTP!' })
  }
  console.log("OTP verified")
  var updateVerification = await Service.getUpdateVerification(data.email)
  if (updateVerification) {
    var mongoId = getUserData && getUserData.length > 0 ? getUserData[0]._id : ''
    var token = await Service.generateToken(mongoId, data.email);

    var userData = {
      email: data.email,
      mobile: getUserData[0].mobile,
      token: token
    }
    return res.status(200).json({
      success: true,
      msg: 'Success! Account Verified!',
      userData: userData
    })
  }
});

//=============Api for login====================//
userRouter.post('/login', async (req, res, next) => {
  var data = req.body ? req.body : ''
  if (!data.email || !data.password) {
    return res.status(200).json({
      success: false,
      msg: 'Please pass email,userType and otp in body'
    })
  }
  var email = req.body.email ? req.body.email : ''
  var password = req.body.password ? req.body.password : ''
  var userData = await Service.emailDuplicacyCheck(email)
  if (!userData || userData.length == 0) {
    return res.status(200).json({
      success: false, msg: 'Error! Account not found!',
    })
  } else if (userData[0].isVeifiedOTP == 'no') {
    return res.status(200).json({
      success: false, msg: 'Error! Account not verified!',
    })
  } else {
    passport.authenticate('local', { session: false }, (err, passportUser, info) => {
      if (err) {
        return next(err);
      }
      if (passportUser) {
        var studentData = passportUser;
        studentData.token = passportUser.generateJWT();
        var auth = studentData.toAuthJSON();
        var obj = {
          _id: studentData._id,
          email: studentData.email,
          token: auth.token,
        }

        return res.status(200).json({
          success: true,
          msg: 'Logged in successfully!',
          data: obj
        })
      }
      return res.status(401).json({
        success: false,
        msg: 'Login Failed!'
      })
    })(req, res, next);
  }
});


//=================API to send OTP for Forgot Password==================//
userRouter.get('/send-passwordOTP/:userId', aeh(async function (req, res) {
  const { userId } = req.params;
  const userData = await User.find({ _id: userId });
  if (!userData || userData.length == 0) {
    return res.status(201).success("User not found");
  }
  const userEmail = userData[0].email
  let mailTransporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'amanraza2507@gmail.com',
      pass: '9289678598'
    }
  });

  var otp = Math.floor(100000 + Math.random() * 900000);
  let mailDetails = {
    from: 'amanraza2507@gmail.com',
    to: userEmail,
    subject: 'Password Reset EPT',
    text: 'OTP for reset password for your account is ' + otp + ' valid for 10 minutes'
  };

  await mailTransporter.sendMail(mailDetails, async function (err, data) {
    if (err) {
      return res.error.success("Error while sending email", err);
    } else {
      var updateOTP = await Service.updatePassResetOPT(userId, otp)
      return res.success("Email Sent Successfully");
    }
  });
}))

//=================API to verify otp for Forgot Password==================//
userRouter.post('/forgot-password/:userId', aeh(async function (req, res) {
  const { userId } = req.params;
  const { otp, newPassword, confirmPassword } = req.body;
  if (newPassword !== confirmPassword) {
    return res.status(200).error("Error! Passwords do not match");
  }
  const userData = await User.find({ _id: userId });
  if (!userData || userData.length == 0) {
    return res.status(201).success("User not found");
  }
  var expireTime = userData[0].resetPassOTPData.expireTime;
  var currentTimeStamp = Math.floor(Date.now() / 1000)
  if ((currentTimeStamp > expireTime) || userData[0].resetPassOTPData.otp !== Number(otp)) {
    return res.status(201).success("Invalid OTP");
  }
  var object = userData[0]
  object.setPassword(newPassword)
  await User.findOneAndUpdate({ _id: userId }, object)
  return res.success("Password Updated successfully!");
}))

//================API to get data of student and teacher for update=================//
userRouter.post('/edit/:id', aeh(async (req, res, next) => {
  var data = req.params ? req.params : ''
  var userType = req.body.userType ? req.body.userType : null;
  if (!data.id || !userType) {
    return res.status(200).json({
      success: false,
      msg: 'Please pass id in params and userType in body'
    })
  }
  var id = data.id ? data.id : ''
  var userData = await Service.getDataById(data.id)

  if (!userData || userData.length == 0) {
    return res.status(200).json({
      success: false, msg: 'Error! Account not found!',
    })
  }
  if (userType == 'student') {
    var userdata = await StudentProfile.find({ studentId: data.id }).populate('studentId')
    // var studentData = await Service.getSudentProfileData(data.id)
  } else if (userType == 'teacher') {
    var userdata = await TeacherProfile.find({ teacherId: data.id }).populate('teacherId')
  }
  res.status(200).json({
    success: true,
    data: userdata
  });
}));

//===============API for updating data=====================//
userRouter.post('/update', aeh(async (req, res, next) => {
  var data = req.body ? req.body : ''
  if (!data.id || !data.userType) {
    return res.status(200).json({
      success: false,
      msg: 'Please pass id,userType body'
    })
  }

  var userData = await Service.getDataById(data.id)
  if (!userData || userData.length == 0) {
    return res.status(200).json({
      success: false, msg: 'Error! Account not found!',
    })
  }
  //validate Email
  var checkEmail = Service.validateEmail(data.email)
  if (!checkEmail) {
    return res.status(200).json({ success: false, msg: 'Error! Email is not Valid!' })
  }
  //validate Phone Number
  var checkPhone = Service.validatePhonenumber(data.mobile)
  if (!checkPhone) {
    return res.status(200).json({ success: false, msg: 'Error! Phone No. is not Valid!' })
  }

  var updateUserData = await User.findOneAndUpdate({ _id: data.id }, { email: data.email, mobile: data.mobile })
  if (data.userType == 'student') {
    var studentProfileUpdate = await StudentProfile.findOneAndUpdate({ studentId: data.id }, { education: data.education, qualification: data.qualification, languages: data.languages })
  }
  else if (data.userType == 'teacher') {
    var TeacherProfileUpdate = await TeacherProfile.findOneAndUpdate({ teacherId: data.id }, { degree: data.degree, description: data.description, subjects: data.subjects })
  }
  return res.status(200).json({ success: true, msg: 'Updated Succesfully' })
}));

//=================API to Re-generate OTP======================//
userRouter.post('/regenerateOTP', async function (req, res) {
  var data = req.body;
  if (!data.email) {
    return res.status(200).json({
      success: false,
      msg: 'Please pass email in body'
    })
  }

  var email = data.email ? data.email : ''
  // var mobile = data.mobile ? data.mobile : ''
  // var userType = data.userType ? data.userType : ''

  var getStudentData = await Service.emailDuplicacyCheck(email)
  if (!getStudentData || getStudentData.length == 0) {
    return res.status(200).json({ success: false, msg: 'Error! Account not found!' })
  }

  // create OTP With Twilio
  // var client = require('twilio')(Config.accountSid, Config.authToken);
  var otp = Math.floor(100000 + Math.random() * 900000);

  // client.messages
  //   .create({
  //     body: 'Your Veification otp is ' + otp + 'valid for 10 minutes',
  //     from: '+19382010415',
  //     to: '+91' + mobile
  //   })
  //   .then(async message => {
  //     console.log(message.sid);
  //     var updateOTP = await Service.getUpdateOPT(email, otp)
  //     return res.status(200).json({ success: true, msg: 'OTP sent successfully!' })
  //   })
  //   .catch((err) => {
  //     console.log("errrrrrrrrrrrrr", err)
  //   })

  let mailDetails = {
    from: 'amanraza2507@gmail.com',
    to: email,
    subject: 'EPT OTP',
    text: 'OTP for account verification is ' + otp + ' valid for 10 minutes'
  };

  await mailTransporter.sendMail(mailDetails, async function (err, data) {
    if (err) {
      return res.error.success("Error while sending email", err);
    } else {
      var updateOTP = await Service.getUpdateOPT(email, otp)
      return res.status(200).json({ success: true, msg: 'OTP regenerated successfully!' })
    }
  });
});


module.exports = userRouter;