const express = require('express');
const router = express.Router();
const aeh = require('../../src/middleware/asyncErrorHandler');
const Conversation = require('../db/models/Conversation');
const Message = require('../db/models/Message');
const TeacherProfile = require('../db/models/TeacherProfile');
const Mongoose = require('mongoose');

//=================API for All Conversations==================//
router.get('/', aeh(async function (req, res) {
  const allConversations = await Conversation.find().sort({ updatedAt: -1 });
  return res.success(null, allConversations);
}));

//==========API to find conversation of specific user=========//
router.get('/users/:userId', aeh(async function (req, res) {
  const { userId } = req.params;
  const allConversations = await Conversation.find({ "members": userId }).sort({ updatedAt: -1 });
  return res.success(null, allConversations);
}));

//============API to create conversation with members=============//
router.post('/', aeh(async function (req, res) {
  if (req.body.type == 'P' || req.body.members.length == 2) {
    const existingConversation = await Conversation.find(req.body);
    if (existingConversation.length > 0) {
      return res.status(201).error("Chat Already Exist", existingConversation);
    }
  }
  const allConversations = await Conversation.create(req.body);
  return res.success("created successfully", allConversations);
}));

//============API to create conversation as per subject=============//
router.post('/subject/:subject', aeh(async function (req, res) {
  const { subject } = req.params
  const { name } = req.body
  const { studentId } = req.body
  const allUsers = await TeacherProfile.find({ subjects: subject }, { teacherId: 1, _id: 0 });
  let members = allUsers.map(docs => { return docs.teacherId })
  members.push(studentId)
  const allConversations = await Conversation.create({ name, studentId, members });
  return res.success("created successfully", allConversations);
}));

//===========API to find messages of a particular conversation============//
router.get('/:conversationId', aeh(async function (req, res) {
  const { conversationId } = req.params;
  const conversation = await Message.find({ conversationId }).populate('author').sort({ 'createdAt': 1 })
  var conversations = conversation.map(doc => doc.toJSON({ virtuals: true }))
  return res.success("Message List", conversations);
}));

//===============API to add members in a conversation============//
// router.post('/:id/add-group-member', aeh(async function (req, res) {
//   const { id } = req.params;
//   const { members } = req.body;
//   const conversation = await Conversation.update({ _id: id }, { $addToSet: { members: members } });
//   return res.success("Member Added successfylly", conversation);
// }));

//===============API to remove members from a conversation============//
// router.post('/:id/remove-group-member', aeh(async function (req, res) {
//   const { id } = req.params;
//   const { members } = req.body;
//   const conversation = await Conversation.update({ _id: id }, { $pull: { members: members } });
//   return res.success("Member Removed successfylly", conversation);
// }));
module.exports = router;