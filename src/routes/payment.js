require('dotenv').config()
const paymentRouter = require('express').Router();
const passport = require('passport');
const instamojo = require('instamojo-nodejs');
const Crypto = require("crypto");
const User = require('../db/models/User');
const Transaction = require('../db/models/Transaction');
const url = require('url');
var Service = require('../services/user.js')
var randomstring = require("randomstring");
var Razorpay = require('razorpay');


var instance = new Razorpay({
    key_id: process.env.RAZORPAY_KEY_ID,
    key_secret: process.env.RAZORPAY_KEY_SECRET
})

//=============API for payment to recharge account RAZORPAY==============//

paymentRouter.route('/pay').post(async function (req, res) {
    const { amount, currency, notes } = req.body
    const payment_capture = 1
    const receipt = randomstring.generate(7)
    await instance.orders.create({ amount, currency, receipt, payment_capture, notes }).then((response) => {
        console.log("**********Order Created***********");
        console.log(response);
        console.log("**********Order Created***********");
        return res.status(200).success("Order Created successfully!", response);
    }).catch((error) => {
        console.log(error);
        return res.status(401).error("Error while order creation!", error);
    })
});

paymentRouter.route('/purchase').post(async function (req, res) {
    req.body = {
        razorpay_payment_id: 'pay_Evl5s5IZrzBUrE',
        razorpay_order_id: 'order_Evl5k5Z1Cvu0yR',
        razorpay_signature: 'dc63eaae06c5ab24f913a39fbbf2ff720e9e3b924f405d369932f5bf3a405ad3'
    }

    // Generate signature to check if its same as generated by razorpay or not
    var generatedSignature = Crypto
        .createHmac(
            "SHA256",
            process.env.RAZORPAY_KEY_SECRET
        )
        .update(req.body.razorpay_order_id + "|" + req.body.razorpay_payment_id)
        .digest("hex");
    if (generatedSignature != req.body.razorpay_signature) {
        return res.status(404).error("Payment Not Authorized!");
    }

    payment_id = req.body;
    console.log("**********Payment authorized***********");
    console.log(payment_id);
    console.log("**********Payment authorized***********");
    await instance.payments.fetch(payment_id.razorpay_payment_id).then(async (response) => {

        const userData = await User.findOne({ "_id": req.body.userId })
        let finalAmount = userData ? userData.walletAmount : 0
        const newAmount = finalAmount + Number(response.amount);
        const transaction = await Transaction.create({ userId: req.body.userId, type: 'C', narration: "Recharge wallet by payment", amount: response.amount, finalAmount: newAmount, orderId: response.order_id, paymentId: response.id });
        const updateUser = await User.updateOne({ _id: req.body.userId }, { walletAmount: newAmount });

        console.log("**********Payment instance***********");
        console.log(response);
        console.log("**********Payment instance***********");
        return res.status(200).success("Payment Successfull!", response);
    }).catch((error) => {
        console.log(error);
    });
})



//=============API for payment to recharge account==============//
// paymentRouter.post('/', async (req, res, next) => {
//     passport.authenticate('user', { session: false }, async (err, user, info) => {
//         if (err) {
//             console.log(err);
//             return next(err);
//         } else {
//             instamojo.setKeys('test_6177bba8d4217059cd11e67b76c', 'test_0c5667848ac02348966f3f559d9');
//             const data = new instamojo.PaymentData();
//             instamojo.isSandboxMode(true);
//             const stud = await Service.getDataById(user.id);

//             data.purpose = req.body.purpose;
//             data.amount = req.body.amount;
//             data.buyer_name = user.name;
//             data.email = user.email;
//             //data.user_id = '5ebbbfbafc320a32845e2052';
//             data.redirect_url = `http://localhost:3000/payment/callback?user_id=${user.id}`;
//             data.phone = user.mobile;
//             data.send_email = false;
//             data.send_sms = false;
//             data.webhook = 'http://www.example.com/webhook/';
//             data.allow_repeated_payments = false;

//             instamojo.createPayment(data, function (error, response) {
//                 if (error) {
//                     return res.json({
//                         success: false,
//                         msg: 'Unable to make payment.'
//                     })
//                 } else {
//                     // Payment redirection link at response.payment_request.longurl
//                     console.log(response);
//                     response = JSON.parse(response)
//                     return res.json({
//                         success: true,
//                         transaction_url: response.payment_request.longurl,
//                         id: user.id,
//                         email: response.payment_request.email
//                     })
//                 }
//             });
//         }
//     })(req, res, next);
// })

// paymentRouter.get('/callback', (req, res) => {
//     let url_parts = url.parse(req.url, true);
//     console.log(url_parts);
// })

//======================== Debit/Credit wallet amount from any account=====================//

paymentRouter.route('/wallet').post(async function (req, res) {
    return new Promise(async function (resolve, reject) {
        var data = req.body ? req.body : ''
        if (!data.userId || !data.type || !data.narration || !data.amount) {
            return res.status(401).error("Error! Please pass userId, type, narration, amount");
        }
        const userData = await User.findOne({ "_id": data.userId })
        let finalAmount = userData ? userData.walletAmount : 0
        let newAmount = 0
        if (data.type == "C") {
            newAmount = finalAmount + Number(data.amount);
        } if (data.type == "D") {
            newAmount = finalAmount - Number(data.amount);
        }
        const transaction = await Transaction.create({ userId: data.userId, type: data.type, narration: data.narration, amount: data.amount, finalAmount: newAmount });
        const updateUser = await User.updateOne({ _id: data.userId }, { walletAmount: newAmount });
        return res.status(200).success("Transaction updated successfully!");
    })
})

//======================== API for transaction history of a user=====================//

paymentRouter.route('/transaction-history/:id').get(async function (req, res) {
    var data = req.params ? req.params : ''
    if (!data.id) {
        return res.status(401).error("Error! Please pass userId in params");
    }
    const userData = await Transaction.find({ userId: data.id }).sort({ createdAt: -1 })
    return res.status(200).success("Transactions found successfully!", userData);
})

module.exports = paymentRouter