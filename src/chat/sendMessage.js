const Messages = require('../db/models/Message');
const User = require('../db/models/User');
var moment = require('moment-timezone');
const currentDate = Date.now()
const date = moment(currentDate).tz('Asia/Calcutta').format('YYYY-MM-DD')
const time = moment(currentDate).tz('Asia/Calcutta').format('hh mm a')

module.exports = (io, socket) => {

  socket.on('sendAlert', async ({ author, body, conversationId, msgType, attachment }) => {
    console.log("inside sendAlert server")
    const localUidDateTime = { date, time }
    const createMessage = await Messages.create({ author, body, conversationId, msgType, attachment, });
    const messageWithAuthor = await Messages.find({ _id: createMessage._id }).populate("author");
    const alertEvent = conversationId + 'A'
    console.log("mmmmmmmmmmmmmmmmmmmm", alertEvent)
    socket.broadcast.emit(alertEvent, "messageWithAuthor[0]");
  });

  socket.on('sendMessage', async ({ author, body, conversationId, msgType, attachment, localUid }) => {
    const localUidDateTime = { date, time }
    const createMessage = await Messages.create({ author, body, conversationId, msgType, attachment, localUid, localUidDateTime });
    const messageWithAuthor = await Messages.find({ _id: createMessage._id }).populate("author");
    socket.broadcast.emit(conversationId, messageWithAuthor[0]);
  });

  socket.on('typing', async ({ author, conversationId, status }) => {
    const username = await User.findOne({ _id: author });
    const data = {
      firstName: username.firstName,
      lastName: username.lastName,
      _id: username._id,
      userName: username.userName,
      place: username.place,
      status: status
    }
    const isTyping = conversationId + 'T'
    socket.broadcast.emit(isTyping, data);
  });

  // Handler for 'received' event
  socket.on('received', async function ({ recieverId, messageId, conversationId }) {
    const messageWithAuthor = await Messages.find({ _id: messageId });

    const delivery = {
      localUid: messageWithAuthor[0].localUid,
      userId: recieverId,
      conversationId,
      messageId,
      date,
      time
    };
    const conversation = await Messages.update({ _id: messageId }, { $addToSet: { delivery: { userId: recieverId, date, time } } });
    const delivered = conversationId + "R"
    socket.broadcast.emit(delivered, delivery);   // Emit 'delivered' event
  });

  // Handler for 'markSeen' event
  socket.on('markSeen', async function ({ recieverId, messageId, conversationId }) {
    const messageWithAuthor = await Messages.find({ _id: messageId });
    const seenData = {
      localUid: messageWithAuthor[0].localUid,
      userId: recieverId,
      conversationId,
      messageId,
      date,
      time
    };
    const conversation = await Messages.update({ _id: messageId }, { $addToSet: { received: { userId: recieverId, date, time } } });
    const markedSeen = conversationId + "S"
    socket.broadcast.emit(markedSeen, seenData);    // Emit 'markedSeen' event
  });
};
