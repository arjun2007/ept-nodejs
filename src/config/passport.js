const passport = require('passport');
const LocalStrategy = require('passport-local');
const User = require('../db/models/User');
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

passport.use('local', new LocalStrategy(
  { usernameField: 'email', passwordField: 'password', session: false },
  (email, password, done) => {
    User.findOne({ email })
      .then((student) => {
        if (!student || !student.validatePassword(password)) {
          return done(null, false, { errors: 'email or password is invalid' });
        }
        return done(null, student);
      })
      .catch(done);
  }
));

const opts = {
  jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme('JWT'),
  secretOrKey: 'secret'
};
passport.use('user',
  new JWTstrategy(opts, async (jwt_payload, done) => {
    try {
      User.findOne({
        _id: jwt_payload.id
      }).then(user => {
        if (user) {
          console.log('user found in db in passport');
          // note the return removed with passport JWT - add this return for passport local
          done(null, user);
        } else {
          console.log('user not found in db');
          done(null, false);
        }
      });
    } catch (err) {
      done(err);
    }
  }),
);